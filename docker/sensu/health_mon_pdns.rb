#!/usr/bin/env ruby
# Ruby sensu check to run and put PureDNS health monitor stats on sensu server
require 'English'

healthstatusfile='/var/log/pdns-hm.log'

def output(*args)
  unless args.empty?
    if args[2].nil?
      args[2] = Time.now.to_i
    end
    puts "PDNS.#{`hostname`.chomp}.pdns.#{args[0..2].join("\s")}"
  end
end

def cmd_run(cmd)
    result = `#{cmd}`.split("\n")
    if $CHILD_STATUS.exitstatus > 0
      puts("Failed to run: #{cmd}")
      exit 1
    end
    return result
end

# We need to parse the health monitor log file.

File.readlines("#{healthstatusfile}")[-4..-1].each do |line|
	output([line.split(',')[1].strip, line.split(',')[3].strip].join("."), line.split(',')[5].strip)
end
