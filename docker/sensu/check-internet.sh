#!/bin/bash
url=google.com
if ping -q -c 1 -W 1 $url >/dev/null; then
  echo "The network is up"
  exit 0
else
  echo "The network is down"
  exit 2
fi
