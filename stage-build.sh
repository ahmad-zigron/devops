#!/bin/bash


function tryexec() {
  cmd=$1
  for x in {{1..3}}; do
    eval $cmd
    if [[ $? -eq 0 ]]; then
      return
    fi
  done
  echo "Failed to execute $cmd"
  exit 1
}
echo "removing old puredns code from docker build directory...."
tryexec "rm -rf /etc/pdns-stage/automationscripts/docker/pdns/*"
echo "fetching latest code from bitbucket....."
tryexec "cd /etc/pdns-stage/pdns-recursor-4.0.5"
tryexec "git fetch"
echo "checking out release develop"
tryexec "git checkout develop"
echo "copying latest code to docker build directory...."
tryexec "cp -r * /etc/pdns-stage/automationscripts/docker/pdns"
echo "building latest dev docker image"
tryexec "cd /etc/pdns-stage/automationscripts/docker"
tryexec "sudo docker build -t ahmadzigron/pdns:develop ."
echo "pushing the latest dev docker image to docker hub.."
tryexec "sudo docker push ahmadzigron/pdns:develop"