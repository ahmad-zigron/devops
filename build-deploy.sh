#!/bin/bash


function tryexec() {
  cmd=$1
  for x in {{1..3}}; do
    eval $cmd
    if [[ $? -eq 0 ]]; then
      return
    fi
  done
  echo "Failed to execute $cmd"
  exit 1
}
echo "removing old puredns code from docker build directory...."
tryexec "rm -rf /etc/pdns-prod/automationscripts/docker/pdns/*"
echo "removing old health monitor code from docker build dir..."
tryexec "rm -rf /etc/pdns-prod/automationscripts/docker/pdns_hm/*"
echo "fetching latest code from bitbucket....."
tryexec "cd /etc/pdns-prod/pdns-recursor-4.0.5"
tryexec "git fetch"
echo "checking out release TAG: $TAG"
tryexec "git checkout $TAG"
echo "copying latest code to docker build directory...."
tryexec "cp -r * /etc/pdns-prod/automationscripts/docker/pdns"
echo "building latest dev docker image"
tryexec "cd /etc/pdns-prod/automationscripts/docker"
tryexec "sudo docker build -t ahmadzigron/pdns:$TAG ."
echo "pushing the latest dev docker image to docker hub.."
tryexec "sudo docker push ahmadzigron/pdns:$TAG"