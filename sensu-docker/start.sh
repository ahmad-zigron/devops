#!/bin/bash
sudo rabbitmqctl add_vhost /sensu
sudo rabbitmqctl add_user sensu mysecret
sudo rabbitmqctl set_permissions -p /sensu sensu ".*" ".*" ".*"
sudo service rabbitmq-server restart
sudo service uchiwa restart